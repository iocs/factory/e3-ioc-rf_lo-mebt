
require essioc
require iocmetadata
require lobox

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("DEVICENAME", "MEBT-010:RFS-LO-010")
epicsEnvSet("IPADDR", "mebt-lob-011.tn.esss.lu.se")

iocshLoad("$(lobox_DIR)/lobox.iocsh")

pvlistFromInfo("ARCHIVE_THIS", "$(DEVICENAME):ArchiverList")
pvlistFromInfo("SAVRES_THIS", "$(DEVICENAME):SavResList")

